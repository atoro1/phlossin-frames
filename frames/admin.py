from django.contrib import admin

from frames import models


@admin.register(models.FrameDesign)
class FrameDesignAdmin(admin.ModelAdmin):
    @admin.display(description="Likes")
    def like_count(self, obj):
        return obj.likes.count()

    list_display = ("__str__", "like_count")
    search_fields = ["sku"]
