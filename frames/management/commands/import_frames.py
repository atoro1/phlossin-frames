from django.core.files import File
from django.core.management.base import BaseCommand
from frames.models import FrameDesign
from pathlib import Path


class Command(BaseCommand):
    help = "Imports frames from image files, including SKU as name"

    def add_arguments(self, parser):
        parser.add_argument("directory", nargs=1)

    def handle(self, *args, **options):
        directory = Path(options["directory"][0])

        for file in directory.glob("*"):
            frame_sku = file.name.split(" ")[1].split("-")[0]
            frame_variant = int(file.name.split(" ")[1].split("-")[1].split(".")[0])

            f = FrameDesign.objects.create(
                sku=frame_sku,
                variation=frame_variant,
                image=File(file.open(mode="rb"))
            )
            print(f"Frame created - {f.id}")
