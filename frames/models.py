from django.db import models
from django.conf import settings


class FrameDesign(models.Model):
    image = models.ImageField()
    sku = models.CharField(max_length=20)
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True)
    premium_votes = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name="premium_votes")
    variation = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return f"SKU {self.sku}-{self.variation:0>2}"

    class Meta:
        ordering = ["sku", "variation"]
