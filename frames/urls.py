from django.urls import path
from frames import views


app_name = "frames"
urlpatterns = [
    path("", views.FramesListView.as_view(), name="frame_list"),
    path("like/", views.frame_like_toggle, name="frame_like"),
    path("vote/", views.frame_vote_toggle, name="frame_vote"),
]
