import json
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views import generic
from frames import models


class FramesListView(LoginRequiredMixin, generic.ListView):
    template_name = "frames/frames_list.html"
    model = models.FrameDesign
    paginate_by = 36
    paginate_orphans = 4

    def get_ordering(self):
        if order := self.request.GET.get("order"):
            return order
        return ["sku", "variation"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["liked_frames"] = models.FrameDesign.objects.filter(likes__in=[self.request.user])
        context["voted_frames"] = models.FrameDesign.objects.filter(premium_votes__in=[self.request.user])
        return context


def frame_like_toggle(request):
    if request.method == "POST":
        try:
            data = json.loads(request.body)
            frames_id = data.get("frames_id")
            frames = models.FrameDesign.objects.get(id=frames_id)
            if request.user in frames.likes.all():
                frames.likes.remove(request.user)
                action = "removed from"
            else:
                frames.likes.add(request.user)
                action = "added to"
            frames.save()
            return JsonResponse({"status": f"Frames {action} likes"})
        except Exception as e:
            return JsonResponse({"status": f"error: {e}"}, status=400)


def frame_vote_toggle(request):
    if request.method == "POST":
        try:
            data = json.loads(request.body)
            frames_id = data.get("frames_id")
            frames = models.FrameDesign.objects.get(id=frames_id)
            if request.user in frames.premium_votes.all():
                frames.premium_votes.remove(request.user)
                action = "removed from"
            else:
                frames.premium_votes.add(request.user)
                action = "added to"
            frames.save()
            return JsonResponse({"status": f"Frames {action} premium votes"})
        except Exception as e:
            return JsonResponse({"status": f"error: {e}"}, status=400)
